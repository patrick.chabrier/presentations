How to

# manage a biblio with pandoc and md?

* https://www.laspic.eu/trucs-et-astuces-pour-la-gestion-des-references-bibliographiques
* https://github.com/KurtPfeifle/pandoc-csl-testing
* https://dillinger.io/
* https://tex.stackexchange.com/questions/435024/with-pandoc-is-it-possible-to-export-a-bibliography-in-markdown
* https://pandoc.org/MANUAL.html#option--to