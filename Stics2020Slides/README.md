# INRAE Beamer Theme

_(Status: alpha)_

An unofficial beamer theme for INRAE.

Usage: Copy `images` directory and `beamerthemeinrae.sty` to your project directory, then write
```
\usetheme{inrae}
```
in your beamer document.
![](example.png)
