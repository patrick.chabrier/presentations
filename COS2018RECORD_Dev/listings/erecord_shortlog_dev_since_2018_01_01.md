# Nathalie Rousse (36):
* updating from deployed erecord : solaid into database
* - Modification of erecord web site entry point : index.html, onlinedoc.html ...     - build_vpzpages.py completed and corrected     - erecord_db/menu.html : added 'accept=".zip" for datafolder value     - urls.py modifications about admin request management
* essai tmp
* essai tmp
* essai tmp
* essai tmp
* essai tmp
* essai tmp
* essai tmp
* essai tmp
* essai tmp
* essai tmp
* New erecord development version : without pyvle (using simulations of erecord pkg), for vle-2 (and maybe several vle versions at the same time, later on), to use cluster.     Chronology of erecord development versions : vle-1.0 < vle-1.1 < vle-x
* cleaning
* Creation of vle-x new erecord developmemt version, by export of previous version (vle-1.1).
* Updating vle-1.1/../003_help_notes according to version installed under erecord VM (147.100.164.34)
* Updating vle-1.1/../002_deployment/history according to version installed under erecord VM (147.100.164.34)
* Updating vle-1.1/../trunk according to version installed under erecord VM (147.100.164.34)
* Updating vle-x/trunk according to local version (ERECORD2) using erecord package instead of pyvle
* Updating vle-x/branches/001_history,002_deployment,003_help_notes according to local version (ERECORD2) using erecord package instead of pyvle
* vle-x/trunk,erecord_slm/models.py : Modifying get_upload_to_path to make migrations possible.
* Adding several vle versions management : adding VleVersion, adding fields to VpzAct (vlevlersion, vleusrpath), adding erecord_conf rule for VpzPath case...
* Minor modifications and corrections, especially to maintain compatibility with previous erecord version (vle-1.1) :     - parameter value types : same values as before (double, boolean...) instead of DOUBLE, BOOLEAN...     - case of empty simulation results     - documentation
* - improving error case in vle_run (stderr.txt file management).     - updating docs/vpzpages (adding vle version, deleting pattern case).     - documentation
* Comments and documentation
* misc (install documentation, minor things...)
* Updating while deploying (debian 9.5, apache2.4)
* Mainly documentation of installation
* Correction to be able to initialize database
* Updating deployment documentation
* Updating urls
* install updating
* - using file based sessions     - adding django_admin_log into admin.py     - adding access.log file production and using     - other minor modifications
* Minor : adding erecord/factory/log/erecord.log, modifying ACTIVITY_PERSISTENCE and DEBUG values.
* vle_2.0.1_precmd.sh added for future vle version
* The production version is now vle-x, online at 147.100.179.168 with DNS erecord
