# Patrick Chabrier (9):
* record.meteo: add a "drias_ascii" format
* record.meteo: enhancing "begin_date" and first line
* pkgs: add record.mipp
* Update MIPP_2CV_CropFull_Server.md
* pkgs/record.mipp: fix
* pkgs/record.mipp: restorea readme file
* pkgs/record.mipp: Update Readme.md
* Update MIPP_2CV_CropFull_Server.md
* mailmap: add

# Ronan Trépos (6):
* WACSgen: add lleida parameter file
* record.optim: update according recursive API
* EnKF: update the use of MetaManager API
* record.meteo: remove useless ctx building
* EnKF: simplify R script
* EnKF: use notice (5) level for log

# Éric Casellas (12):
* misc/rvle.record : update vle_home for 2.0
* misc : more explicit outputs for PackagesDependecies
* GenCSVcan : remove gdal search in cmake
* wwdm & UseFortranLib : set default VLE_DEBUG to 0
* gluePhysic : initValue not in extension API anymore
* spudgro : bump to vle-2.0
* carbone & carboneR : bump to vle-2.0
* flood_wave package deprecated
* DateTime: fix API change of vle::utils::DateTime::toTime
* all: switch to vle 2.1
* all: switch to vle 2.1
* fix Description.txt project name
