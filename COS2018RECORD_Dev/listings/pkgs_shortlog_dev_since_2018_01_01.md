# Gauthier Quesnel (10):
* README: swithing http to https the vle-project access
* build: adding exit status
* gitignore: add buildvle*
* packages: fixing multiples problem in cmake script
* travis: adding ci for ubuntu 14.04/gcc-5
* appveyor: adding continuous integration for WinAPI
* travis: use defined environment variables
* tester: replace c to c++ header
* README: adding dependencies
* travis: adding Qt5 dependencies

# Patrick Chabrier (11):
* vle.discrete-time.decision: fix the format func usage
* vle.examples: add an include
* vle.extension.decision: fix a test on resources
* vle.extension.decision: fix tests on resources
* discrete-time.decision: fix the deadline rule
* gvle.decision: update the configuration generated
* gvle.decision: fix the rule management
* decision: fix and add test
* vle.discrete-time: fix ValueVle observation
* vle.tester_test: adding a test of string value
* mailmap: update

# Ronan Trépos (23):
* vle.recursive: few improvments
* vle.recursive: split API
* vle.recursive: add the possibility to modify conds
* vle.tester: use spawn simulation
* add missing vpm
* vle.recursive: fix R plots
* vle.recursive: improve R script
* vle.recursive: allow spawn config
* vle.recursive: fix R script
* gvle.* : fix gvle.discrete-time labels
* vle.recursive: update reading of cvle output.
* update bash script
* switch to vle 2.1
* add backward compatibility with vle 2.0
* all: fix the use of pkg-check-modules in FindVLE
* gvle.discrete-time: use of new code editor widgets
* vle.tester: improve error management
* improve R script
* vle.recursive: fix R script
* vle.recursive: update the use of cvle
* gvle.discrete-time: add backward comp. with vle 20
* vle.recursive: fix the computation of MSE
* vle.recursive: use info (6) level for log

# Éric Casellas (4):
* build script : fix pkgs dependencies order (build & test)
* vle.extension.decision : fix Trace usage
* gvle.simulating.plan: fix virtual usage
* gvle.simulating.plan: fix API change of utils::Context::LogFunctor
