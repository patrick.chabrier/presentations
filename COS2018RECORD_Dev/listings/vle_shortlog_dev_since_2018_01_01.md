# Gauthier Quesnel (109):
* 2018: bump the year
* gvle: fix build since QString::vasprintf availabled from QT5.5
* appveyor: enable Windows build
* template: fixed bad installation notice
* utils: remove old style exception qualifier
* windows: switch windows API to unicode
* qmake: update file
* cpack: rename the final nsis executable
* appveyor: update with fully package build
* template: improved find script
* vpz: fixed parsing of observation type
* win32: removed the use of CPack + NSIS
* vpz: fixed a conversion error
* vle-2.0.0-beta1
* cmake: remove old valgrind suppression list
* win32: updated documentation and scripts
* cmake: replaced old win32 documentation
* appveyor: updated to automatically produce the Win32 installer
* README: improved links to appveyor
* devs: replaced Trace macro with function
* devs: added two Trace functions in Dynamics class
* all: fixed implicit integer/real conversion
* vpz: removed old GVLE 1.x functions
* utils: simplified the DateTime API
* all: updated source code
* cvle: updated coding style
* cvle: removed unused variables
* cvle: manage empty columns in cvle plan
* vle: removed boost::mpi boost::serialization dependencies
* cvle: replaced boost::mpi source code with mpi.h API
* cvle: fixed csv template generation
* devs: fixed visibility of the global Trace function
* cvle: fixed bad access to indices vectors
* value: fixed csv output stream for Map and Matrix
* cvle: fixed csv format generation
* travis: added gcc 6, 7 and 8
* utils: fixed compilation error with gcc >= 6
* travis: removed gcc 8
* utils: fixed DataTime buffer
* utils: fixed a catch code
* utils: fixed use of noexcept in swap member
* cvle: added missing standard header
* cvle: fixed compilation error with gcc >= 8
* travis: re-enabled gcc 8
* cvle: added row id in simulation name
* devs: enabled the instance in plug-in output
* cvle: added row id in output file
* travis: enable cvle and mvle build
* travis: replace libopenmpi 1.6 with libmpich 3
* devs: fixed Trace function output
* cli: protects access to log attribute
* aqss: adds some new line in Trace functions
* vle: modernize source code
* utils: remove useless typepdef
* vpz: remove useless typepdef
* gvle: add a popup/notification widget
* gvle: throw exception if domparser fails
* gvle: fixes invalid vpz detection
* gvle: fixes vpz xml structures in test
* utils: remove C macro from context
* aqss: fix integer cast from std::size_t to long
* all: add ciso646 standard header
* test: fix integer cast from size_t to long
* fs: fix integer cast from size_t to DWORD
* test: remove useless code in C++ version > 11
* fs: fix header for both mingw and vs
* win32: hides implementation details in cpp
* iss: fix installation paths for opengl libraries
* cmake: fix windows port with UtilsWin in cpp file
* utils: fix context with local initialization for win32
* win32: fix API/ABI problem between Qt and Rtools gcc
* utils: add missing header algorithm
* vle-2.0.0-beta2
* mailmap: update
* cli: add condition parameter to update condition's value
* cli: add option to change experiment name
* vle: update manpage
* context: change verbosity level of Context initialization
* vle-2.0.0
* cmake: bump to 2.1.0-devel
* man: add vle, cvle, mvle asciidoc manpage
* doc: add csv style for asciidoc files
* cmake: enable the GNU install directory path
* cmake: enable the build of manpages from asciidoc files
* cmake: remove old manpages
* README: add asciidoctor as dependency to build doc
* i18n: bump pot file
* vle: add a version header
* vle: switch url from http to https
* utils: fix std::vector<T>::operator[] use with 0-length
* vpz: replace std::vector<char> with std::unique_ptr<char[]>
* utils: replace std::vector<char> with std::unique_ptr<char[]>
* utils: adding a subdirectory VLE-%d in VLE_HOME directory
* spawn: fix a fallthrough in waitpid switch/case
* i18n: bump fr.po
* utils: fix windows spawn features
* context: adds version ABI between VFL and subprocess
* template: update FindVLE to latest VLE 2.x features
* appveyor: fix the source path
* utils: replacing old preprocessor identifier
* vle: fixing some mingw64 warnings
* win32: updating build and runtime dependencies
* appveyor: adding openssl dependency
* appveyor: updating static/runtime dependencies
* appveyor: fixing bad ssl file access
* context: adding -L option to the curl command line
* cmake: trying to remove the libintl dependency
* vpz: removing xmlXPath functions dependencies
* vpz: replace strt function with basic boost::spirit

# Patrick Chabrier (8):
* cvle: fix the man installation
* vle: fix the usage of "qmake -query"
* gvle: update qcustomplot to Version: 2.0.0
* gvle: enhancing zoom for the simulation panel
* gvle: add a DEVS diagram SVG export
* gvle: add a file loading control
* utils: fix the vle_home path managment
* gvle: add a button to reset the vle configuration file.

# Ronan Trépos (19):
* cmake: fix the use of WITH_DEBUG for static lib
* cvle: add more-output-details option
* cmake: new VLE version management in packages
* package: build pkg in buildvle{VLE_VERSION}
* gvle: fix the use of logger in context log functor
* utils: remove useless log in DownloadManager
* utils: use context log functor
* gvle.default: improve DefaultSimSubpanel logs
* gvle: update record distrib path
* gvle widgets: improve text editors
* gvle widgets: allow the use of undo/redo of QText
* gvle DefaultCppPanel: use of GvleCodeEdit
* asciidoc: does not execute if executable not found
* gvle: fix the copy/paste of atomic model
* cvle : fix the use of more_output_details option
* gvle: fix reference to unknown namespace
* context: fix the use of VLE_DISABLE_DEBUG option
* utils: remove debug message
* utils: fix the Context::log fcts

# Éric Casellas (2):
* gvle: added console log feedback
* cmake : fix vle pkgs path
